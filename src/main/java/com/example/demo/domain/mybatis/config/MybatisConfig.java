package com.example.demo.domain.mybatis.config;

import org.mybatis.spring.annotation.MapperScan;

/**
 * @author yuanxin
 * @create 2020/11/11 11:25
 */
@MapperScan("com.example.demo.domain.mybatis")
public class MybatisConfig {

}
